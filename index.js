const express = require('express')
var app = express();
var bodyParser = require('body-parser');
var mqtt = require('mqtt');
var url = require('url');
var moment = require('moment');

app.set('port', (process.env.PORT || 8080));

app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(express.static('public'))

app.post("/endpoints/sipgate", function (request, response) {
  var client = mqtt.connect(process.env.CLOUDMQTT_URL);

  var from = request.body.from;
  var to = request.body.to;
  var direction = request.body.direction;

  client.on('connect', function () {
    if (direction == 'in') {
      client.publish('homeassistant/sipgate/call/in', moment().toISOString() + ", +" + from + ", +" + to);
    } else if (direction == 'out') {
      client.publish('homeassistant/sipgate/call/out', moment().toISOString() + ", +" + from + ", +" + to);
    }
    
    client.publish('homeassistant/sipgate/call', moment().toISOString() + ", " + direction + ", +" + from + ", +" + to);
  });

  console.log("from: " + from);
  console.log("to: " + to);
  console.log("direction: " + direction);

  response.send("OK");
});

app.get("/_healthz", function (request, response) {
  response.send("OK");
});

app.get("/", function (request, response) {
  response.send("OK");
});

app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});
